<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S02: Repetition Control Structures and Array Manipulation</title>
</head>
<body>

<!-- =============================Activity 3 ==========================-->
<h1>Divisible of Five</h1>
<p><?= forLoop() ?></p>


<!-- =============================Activity 4 ==========================-->
<h1>Array Manipulation</h1>
<?php array_push($students, 'John Smith');?>
<pre><?= var_dump($students); ?></pre>
<pre><?php echo count($students); ?></pre>

<?php array_push($students, 'Jane Smith');?>
<pre><?= var_dump($students); ?></pre>
<pre><?php echo count($students); ?></pre>

<?php array_shift($students);?>
<pre><?= var_dump($students); ?></pre>
<pre><?php echo count($students); ?></pre>

</body>
</html>
